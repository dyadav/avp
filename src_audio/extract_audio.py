# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import argparse
from moviepy.editor import *

parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
args = parser.parse_args()

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, r'data')
PATH_VIDEOS = os.path.join(PATH_PROJECT, r'videos')
PATH_VIDEO = os.path.join(PATH_VIDEOS, args.video_name)

# Step 1 : extract audio
video = VideoFileClip(PATH_VIDEO)
audio = video.audio
path_save = os.path.join(PATH_DATA, args.video_name, 'audio.mp3')
audio.write_audiofile(path_save, fps=22050)

# Step 2 : convert into .wav format
path = os.path.join(PATH_DATA, args.video_name)
audio = path + '/audio.mp3'
audio_save = path + '/audio.wav'
command = 'ffmpeg -i ' + '"' + audio + '" ' + '"' + audio_save + '"'
os.system(command)
