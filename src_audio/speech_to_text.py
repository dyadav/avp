# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import speech_recognition as sr

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
PATH_VIDEOS = os.path.join(PATH_PROJECT, 'videos')
videos = os.listdir(PATH_VIDEOS)[:1]

r = sr.Recognizer()
for video_name in videos:
    if video_name in os.listdir(PATH_DATA):
        path = os.path.join(PATH_DATA, video_name)
        audio_path = path + '/audio.wav'
        video_audio = sr.AudioFile(audio_path)
        with video_audio as source:
            audio = r.record(source, duration=100)
            print(r.recognize_google(audio, language='fr-FR', show_all=True))

