# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import json
from tqdm import tqdm
import time
import pdb
import argparse
from logger import logger
import numpy as np
from func import *
np.random.seed(123)


# Implement K-means algorithm (K-centroids) given 1-IoU metric

class Kmeans:

    def __init__(self, bb_boxes, k, n_iter=100):

        self.bb_boxes = bb_boxes
        self.k = k
        self.n_iter = n_iter
        np.random.shuffle(self.bb_boxes)
        self.centroids = self.bb_boxes[:self.k]
        self.mean_residual = [0] * self.k
        self.count_lab = [0] * self.k

        self.bb_labels = {}
        self.fit()

    def compute_IoU(self):
        self.bb_labels = [np.argmin([1 - IoU(bb, centroid) for centroid in self.centroids]) for bb in self.bb_boxes]
        # print(self.bb_labels.values())

    def fit(self):
        for _ in tqdm(range(self.n_iter)):
            t0 = time.time()
            self.compute_IoU()
            pdb.set_trace()
            candidates = [[[0, 0], [0, 0]] for _ in range(self.k)]
            self.count_lab = [0] * self.k
            for bb, lab in enumerate(self.bb_labels):
                self.count_lab[lab] += 1
                self.mean_residual[lab] += 1 - IoU(self.bb_boxes[bb], self.centroids[lab])
                candidates[lab][1][0] += self.bb_boxes[bb][1][0]
                candidates[lab][1][1] += self.bb_boxes[bb][1][1]

            pdb.set_trace()
            _centroids = [[[0, 0], [0, 0]] for _ in range(self.k)]
            for j, cand in enumerate(candidates):
                if self.count_lab[j]:
                    cand[1][0] = int(cand[1][0]/self.count_lab[j])
                    cand[1][1] = int(cand[1][1]/self.count_lab[j])
                    _centroids[j] = cand
                else:
                    _centroids[j] = self.centroids[j]
            self.centroids = _centroids

            self.mean_residual = [res/self.count_lab[i] for i, res in enumerate(self.mean_residual)
                                  if self.count_lab[i]]

        print('Mean residual : {}'.format(np.mean(self.mean_residual)))
        print('Count lab : {}'.format(self.count_lab))
        print('Time elapsed : {} seconds'.format(time.time() - t0))


    def predict(self, bbox):

        res = []
        for lab, centroid in enumerate(self.centroids):
            res.append(1 - IoU(bbox, centroid))

        return np.argmin(res)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--k', type=int, default=6)
    parser.add_argument('--n_iter', type=int, default=100)
    parser.add_argument('--debug', type=int, default=0)
    args = parser.parse_args()

    if not args.debug:
        pdb.set_trace = lambda: None

    PATH_PROJECT = os.path.dirname(os.getcwd())
    PATH_DATA = os.path.join(PATH_PROJECT, 'data')
    PATH_TRAIN = os.path.join(PATH_PROJECT, 'train_set', 'train_set.json')

    with open(PATH_TRAIN, 'r') as fj:
        data = json.load(fj)

    data = [format_data(v) for v in data.values() if v[0]]
    bb_boxes = get_bb(data)
    bb_boxes = [standardized_bb(bb) for bb in bb_boxes]
    bb_boxes = [bb for bb in bb_boxes if (bb[1][0] > 0) and (bb[1][1] > 0)]  # remove wrong bboxes

    kmeans = Kmeans(k=args.k, bb_boxes=bb_boxes, n_iter=args.n_iter)
    print(kmeans.centroids)
    print(kmeans.mean_residual)
    print(kmeans.count_lab)



















