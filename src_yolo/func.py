# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""


def area(h, w):
    return h * w


def bb_to_point(bb):
    center_x, center_y = bb[1][0], bb[1][1]
    h, w = bb[2][0], bb[2][1]
    return (int(center_x - w/2), int(center_y - h/2)), (int(center_x + h/2), int(center_y + h/2))


def IoU(bb_1, bb_2):

    '''
    A means top left coordinate, B means bottom right
    :param bb_1: first bounding boxes
    :param bb_2: second
    :return: IoU (between 0 and 1)
    '''

    x_A_bb1, y_A_bb1, x_B_bb1, y_B_bb1 = bb_1[0][0], bb_1[0][1], bb_1[1][0], bb_1[1][1]
    x_A_bb2, y_A_bb2, x_B_bb2, y_B_bb2 = bb_2[0][0], bb_2[0][1], bb_2[1][0], bb_2[1][1]

    x_A, y_A = max(x_A_bb1, x_A_bb2), max(y_A_bb1, y_A_bb2)
    x_B, y_B = min(x_B_bb1, x_B_bb2), min(y_B_bb1, y_B_bb2)

    width, height = max(0, x_B - x_A), max(0, y_B - y_A)

    intersec = width * height

    area_bb_1 = area(x_B_bb1 - x_A_bb1, y_B_bb1 - y_A_bb1)
    area_bb_2 = area(x_B_bb2 - x_A_bb2, y_B_bb2 - y_A_bb2)

    return intersec / (area_bb_1 + area_bb_2 - intersec)


def format_data(lab):
    '''
    Transform the data format from [lab, [center_x, center_y], [h, w]] to [lab, [x_right, y_right], [x_left, y_left]]
    :param lab: label from the dataset
    :return: label formatted
    '''
    if lab[0]:
        n_box = len(lab) // 3
        lab_hw = []
        for i in range(n_box):
            label = lab[3*i: 3*(i + 1)]
            lab_hw += bb_to_point(label)
        return lab_hw
    else:
        return lab


def get_bb(data):
    bb_list = []
    for bb in data:
        if len(bb) == 2:
            bb_list.append(bb)
        else:
            n_box = len(bb) // 2
            for idx in range(n_box):
                bb_list.append(bb[idx*2: (idx+1)*2])
    return bb_list


def standardized_bb(bb):
    '''
    left corner at (0, 0) point
    :param bb: bbox
    :return: bbox standardized
    '''
    assert len(bb) == 2
    x, y = bb[0][0], bb[0][1]
    bb_std = []
    for coord in bb:
        bb_std.append((coord[0] - x, coord[1] - y))
    return bb_std

