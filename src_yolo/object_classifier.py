# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import re
import cv2
import glob
import json
import pdb
import argparse
from tqdm import tqdm
from logger import logger
import numpy as np
import torch
import torch.optim as optim
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import TensorDataset
import torch.nn as nn
import torchvision.models as models
from pytorchtools import EarlyStopping
from uuid import uuid4
from func import *
np.random.seed(42)


def main(train_set, dev_set, test_set, lr=1e-5, epochs=5, batch_size=8, model=None,
         early_stopping=1, patience=5, RUN_ID=None):

    if early_stopping:
        early_stopping = EarlyStopping(patience=patience, RUN_ID=RUN_ID, verbose=True)

    # create datasets

    num_train = len(train_set)
    num_dev = len(dev_set)
    train_idx, valid_idx = list(range(num_train)), list(range(num_dev))
    np.random.shuffle(train_idx)
    np.random.shuffle(valid_idx)

    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)

    # load training data in batches
    train_loader = torch.utils.data.DataLoader(train_set,
                                               batch_size=batch_size,
                                               sampler=train_sampler,
                                               num_workers=0)

    # load validation data in batches
    valid_loader = torch.utils.data.DataLoader(dev_set,
                                               batch_size=batch_size,
                                               sampler=valid_sampler,
                                               num_workers=0)

    optimizer = optim.Adam(model.parameters(), lr=lr)
    criterion = nn.CrossEntropyLoss()

    epoch_loss, val_loss = [], []
    accuracy = {'train': [], 'val': [], 'test': []}

    for epoch in range(epochs):

        logger.info('Epoch {} - Start processing'.format(epoch))

        model.train()
        batch_loss = []
        acc = []

        for batch, (X_sample, target) in tqdm(enumerate(train_loader, 1)):

            if torch.cuda.is_available():
                X_sample = X_sample.to(device)
                target = target.to(device)

            pdb.set_trace()

            optimizer.zero_grad()
            output = model.forward(X_sample)
            loss = criterion(output, target)
            loss.backward()
            optimizer.step()

            batch_loss.append(loss.item())
            acc.append(torch.sum(torch.argmax(output, 1) == target).item() / batch_size)

        current_loss = np.mean(batch_loss)
        current_accuracy = np.mean(acc)
        accuracy['train'].append(current_accuracy)
        epoch_loss.append(current_loss)

        logger.info("Train Sample - Epoch {} - Loss : {:.5f} - Accuracy : {:.3f}".format(epoch + 1, current_loss,
                                                                                         current_accuracy))

        model.eval()
        val_acc = []
        val_batch_loss = []
        # pdb.set_trace()

        for data, target in tqdm(valid_loader):

            if torch.cuda.is_available():
                data = data.to(device)
                target = target.to(device)

            output = model.forward(data)
            loss = criterion(output, target)
            val_batch_loss.append(loss.item())
            val_acc.append(torch.sum(torch.argmax(output, 1) == target).item() / batch_size)

        valid_loss = np.mean(val_batch_loss)
        val_loss.append(valid_loss)
        accuracy['val'].append(np.mean(val_acc))

        logger.info("Validation Sample - Epoch {} - Loss : {:.5f} - Accuracy : {:.3f}".format(epoch + 1, valid_loss,
                                                                                              np.mean(val_acc)))


        '''
        Early Stopping
        '''
        early_stopping(valid_loss, model)
        if early_stopping.early_stop:
            print("Early stopping")
            break

    test_loader = torch.utils.data.DataLoader(test_set,
                                              batch_size=batch_size,
                                              num_workers=0)

    model.eval()
    test_acc = []
    test_batch_loss = []

    for data, target in tqdm(test_loader):

        if torch.cuda.is_available():
            data = data.to(device)
            target = target.to(device)

        output = model.forward(data)
        loss = criterion(output, target)
        test_batch_loss.append(loss.item())
        test_acc.append(torch.sum(torch.argmax(output, 1) == target).item() / 32)

    test_loss = np.mean(test_batch_loss)
    accuracy['test'].append(np.mean(test_acc))

    logger.info("Test Sample - Loss : {:.5f} - Accuracy : {:.3f}".format(test_loss, np.mean(test_acc)))

    return model, epoch_loss, val_loss, test_loss, accuracy


if __name__ == '__main__':

    RUN_ID = str(uuid4())[0:5]

    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--batch_size', type=int, default=16)
    parser.add_argument('--lr', type=float, default=1e-4)
    parser.add_argument('--early_stopping', type=int, default=1)
    parser.add_argument('--patience', type=int, default=5)
    parser.add_argument('--dropout', type=float, default=0.2)
    parser.add_argument('--debug', type=int, default=0)
    args = parser.parse_args()

    if not args.debug:
        pdb.set_trace = lambda: None

    print(RUN_ID)
    print('epochs : {}'.format(args.epochs))
    print('batch_size : {}'.format(args.batch_size))
    print('lr : {}'.format(args.lr))
    print('Dropout : {}'.format(args.dropout))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    PATH_PROJECT = os.path.dirname(os.getcwd())
    PATH_DATA = os.path.join(PATH_PROJECT, 'data')
    PATH_TRAIN = '/scratch/mfuteral/avp/train_set/small_train_set.json'
    PATH_DEV = '/scratch/mfuteral/avp/dev_set/small_dev_set.json'
    PATH_TEST = '/scratch/mfuteral/avp/test_set/small_test_set.json'

    video_names = os.listdir(PATH_DATA)
    try:
        video_names.remove(r'.DS_Store')
    except:
        pass

    assert len(video_names) == 8


    def open_jfile(path):
        with open(path, 'r') as fj:
            return json.load(fj)


    train_ids = open_jfile(PATH_TRAIN)
    dev_ids = open_jfile(PATH_DEV)
    test_ids = open_jfile(PATH_TEST)


    def transform_ids(ids):
        return {k: v[0] if len(v) == 3 else np.random.choice([v[0], v[3]]) for k, v in ids.items()}


    train_ids = transform_ids(train_ids)
    n_labels = {0: 0, 1: 0, 2: 0}
    for v in train_ids.values():
        n_labels[v] += 1
    print('Train set\n{}% - Label 0 \n {}% - Label 1 \n {}% - Label 2'.format(n_labels[0]/len(train_ids),
                                                                              n_labels[1]/len(train_ids),
                                                                              n_labels[2]/len(train_ids)))
    dev_ids = transform_ids(dev_ids)
    test_ids = transform_ids(test_ids)


    def load_img(path):
        fname = os.listdir(os.path.dirname(path) + "/small_frames")
        fname = [re.sub("frame", "", name) for name in fname]
        return fname, np.array([cv2.imread(f_img) for f_img in glob.glob(os.path.dirname(path) + "/small_frames/*.jpg")])

    logger.info('Start loading images')
    train_fname, train_img = load_img(PATH_TRAIN)
    dev_fname, dev_img = load_img(PATH_DEV)
    test_fname, test_img = load_img(PATH_TEST)
    logger.info('Images loaded')

    lab_train = torch.Tensor([train_ids[re.sub("s.*", "", k)] for k in train_fname]).long()
    lab_dev = torch.Tensor([dev_ids[re.sub("s.*", "", k)] for k in dev_fname]).long()
    lab_test = torch.Tensor([test_ids[re.sub("s.*", "", k)] for k in test_fname]).long()

    train_img = torch.Tensor(train_img).permute(0, 3, 1, 2)
    dev_img = torch.Tensor(dev_img).permute(0, 3, 1, 2)
    test_img = torch.Tensor(test_img).permute(0, 3, 1, 2)

    train_set = TensorDataset(train_img, lab_train)
    dev_set = TensorDataset(dev_img, lab_dev)
    test_set = TensorDataset(test_img, lab_test)

    resnet34 = models.resnet34(pretrained=True, progress=True)

    class Model(nn.Module):
        def __init__(self, dropout):
            super(Model, self).__init__()
            self.resnet = resnet34
            self.dropout = nn.Dropout(dropout)
            self.layer1 = nn.Linear(1000, 512)
            self.layer2 = nn.Linear(512, 128)
            self.layer3 = nn.Linear(128, 3)

        def forward(self, input):
            x = self.resnet(input)
            x = self.layer1(self.dropout(x))
            x = self.layer2(self.dropout(x))
            out = self.layer3(self.dropout(x))
            return out

    model = Model(args.dropout)

    model, epoch_loss, valid_loss, test_loss, accuracy = main(train_set, dev_set, test_set, lr=args.lr, epochs=args.epochs,
                                                              batch_size=args.batch_size, model=model,
                                                              early_stopping=args.early_stopping, patience=args.patience,
                                                              RUN_ID=RUN_ID)









