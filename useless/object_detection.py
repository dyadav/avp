# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import cv2
import numpy as np

def nothing(x):
    pass

cap = cv2.VideoCapture(0)
cv2.namedWindow('Tracking')

cv2.createTrackbar('L - H', 'Tracking', 0, 179, nothing)
cv2.createTrackbar('L - S', 'Tracking', 0, 255, nothing)
cv2.createTrackbar('L - V', 'Tracking', 0, 255, nothing)
cv2.createTrackbar('U - H', 'Tracking', 179, 179, nothing)
cv2.createTrackbar('U - S', 'Tracking', 255, 255, nothing)
cv2.createTrackbar('U - V', 'Tracking', 255, 255, nothing)


# create old frame
_, frame = cap.read()

while True:
    _, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    l_h = cv2.getTrackbarPos('L - H', 'Tracking')
    l_s = cv2.getTrackbarPos('L - S', 'Tracking')
    l_v = cv2.getTrackbarPos('L - V', 'Tracking')
    u_h = cv2.getTrackbarPos('U - H', 'Tracking')
    u_s = cv2.getTrackbarPos('U - S', 'Tracking')
    u_v = cv2.getTrackbarPos('U - V', 'Tracking')


    lower_green = np.array([l_h, l_s, l_v])
    upper_green = np.array([u_h, u_s, u_v])
    mask = cv2.inRange(hsv, lower_green, upper_green)

    result = cv2.bitwise_and(frame, frame, mask=mask)

    cv2.imshow('frame', frame)
    cv2.imshow('mask', mask)
    cv2.imshow('result', result)

    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()
