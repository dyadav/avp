# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import cv2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
args = parser.parse_args()

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'avp', 'data')
PATH_AVP = os.path.join(PATH_PROJECT, r'AvP - Ressources')
PATH_SESSION = os.path.join(PATH_AVP, r'2v1 Corridor')
PATH_VIDEO = os.path.join(PATH_SESSION, r'Son _ Vidéo', args.video_name)
PATH_SET = os.path.join(PATH_DATA, args.video_name)

if not os.path.exists(PATH_DATA):
    os.makedirs(PATH_DATA)
if not os.path.exists(PATH_SET):
    os.makedirs(PATH_SET)

cap = cv2.VideoCapture(PATH_VIDEO)
frameRate = cap.get(cv2.CAP_PROP_FPS)

success, image = cap.read()

count = 0
while success:
    frameId = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
    if frameId % int(frameRate/4) == 0:
        cv2.imwrite(PATH_SET + "/frame{:.4f}s.jpg".format(frameId/frameRate), image)     # save frame as JPEG file
        print('Read a new frame: ', success)
        print(count)
    success, image = cap.read()
    count += 1






