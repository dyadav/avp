# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import re
import json
import time
import cv2
import argparse

PATH_PROJECT = os.getcwd()
PATH_DATA = os.path.join(PATH_PROJECT, 'data')

parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str)
parser.add_argument('--ms', type=int, default=100)
args = parser.parse_args()

PATH_VIDEO = os.path.join(PATH_DATA, args.video_name)
PATH_FRAMES = os.path.join(PATH_VIDEO, 'labelled_frames')
PATH_LABELS = os.path.join(PATH_PROJECT, 'object_detection.json')
frames = os.listdir(PATH_FRAMES)
try:
    frames.remove('.DS_Store')
except:
    pass

with open(PATH_LABELS, 'r') as fj:
    object_detection = json.load(fj)

data = object_detection["video_name"][args.video_name]["metadata"]
data_sorted = {float(re.sub(r's.jpg', '', k)): v for k, v in data.items()}

colors = [None, (0, 0, 255), (255, 255, 0)]
for i, t in enumerate(sorted(data_sorted.keys()), 1):
    str_t = str(round(t, 4))
    n_figures = len(str_t.split('.')[-1])
    str_t += '0' * (4 - n_figures)
    if i % 200 == 0:
        print(i)
    path = PATH_FRAMES + '/' + str_t + 's.jpg'
    img = cv2.imread(path)

    if data_sorted[t][0] != 0:
        color = colors[data_sorted[t][0]]
        img = cv2.rectangle(img, tuple(data_sorted[t][1]), tuple(data_sorted[t][2]), color=color, thickness=2)
        if len(data_sorted[t]) == 6:
            _color = colors[data_sorted[t][3]]
            img = cv2.rectangle(img, tuple(data_sorted[t][4]), tuple(data_sorted[t][5]), color=_color, thickness=2)

    cv2.imshow('Image', img)
    key = cv2.waitKey(args.ms)

    if key == ord('p'):
        print(i)
        cv2.waitKey(0)

cv2.destroyAllWindows()


