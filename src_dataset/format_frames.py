# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import re
import argparse
import json


parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
args = parser.parse_args()


subpath = os.path.dirname(os.getcwd())
path_data = os.path.join(subpath, 'data')
path_video = os.path.join(path_data, args.video_name)
path_frames = os.path.join(path_video, "new_frames")
frames = os.listdir(path_frames)

ordered_frames = {int(re.sub('[A-Za-z]|\.', '', fname)): fname for fname in frames}
frames_time = {j: (j-1)*0.5 for j in ordered_frames.keys()}
time_frames = {v: k for k, v in frames_time.items()}

with open(os.path.join(os.path.dirname(os.getcwd()), 'event_dataset.json'), 'r') as fj:
    dataset = json.load(fj)

for i, t in enumerate(dataset[args.video_name].keys()):
    start_time = dataset[args.video_name][t]['startTime']
    end_time = dataset[args.video_name][t]['endTime']
    frames_id = [v for k, v in time_frames.items() if float(start_time) - 1.0 <= k <= float(end_time) + 1.0]
    frames_name = [ordered_frames[k] for k in frames_id]
    frames_path = [os.path.join(path_frames, fname) for fname in frames_name]
    frames_path = [re.sub(r'/Volumes/LaCie/Inria/vizion/avp', r'.', path) for path in frames_path]
    dataset[args.video_name][t]['frames'] = '\t'.join(frames_path)


with open(os.path.join(os.path.dirname(os.getcwd()), 'event_dataset.json'), 'w') as fj:
    json.dump(dataset, fj)















