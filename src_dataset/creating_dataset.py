# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import cv2
import argparse
import json
import time


parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
parser.add_argument('--time', type=float, default=0.0)
parser.add_argument('--max_bb', type=int, default=3)
args = parser.parse_args()


PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
PATH_VIDEO = os.path.join(PATH_PROJECT, r'videos', args.video_name)
PATH_SAVE = os.path.join(PATH_DATA, args.video_name)
PATH_SAVE_FRAME = os.path.join(PATH_SAVE, 'frames')

if not os.path.exists(PATH_SAVE):
    os.mkdir(PATH_SAVE)

if not os.path.exists(PATH_SAVE_FRAME):
    os.mkdir(PATH_SAVE_FRAME)

# Format json
data = {}
if os.path.exists(os.path.join(PATH_SAVE, 'labels.json')):
    with open(os.path.join(PATH_SAVE, 'labels.json'), 'r') as f:
        data = json.load(f)

print("When detecting a moving object, please press 'p' to make video pause. Then, left click at top left of the object,"
      " press enter, left click at the bottom right of the object and press enter to continue the video")
time.sleep(5.0)
print("To end the video press 'ESC' ")
print("Video will begin in 3 seconds...")
for i in range(3):
    time.sleep(1.0)
    print(3-i)

# Video treatment
cap = cv2.VideoCapture(PATH_VIDEO)
frameRate = cap.get(cv2.CAP_PROP_FPS)

# Mouse function
def select_point(event, x, y, flags, params):
    global point, point_selected
    if event == cv2.EVENT_LBUTTONDOWN:
        point = (x, y)
        point_selected = True

point_selected = False
point = ()

cv2.namedWindow('frame')
cv2.setMouseCallback('frame', select_point)

if args.time:
    cap.set(cv2.CAP_PROP_POS_FRAMES, args.time*frameRate)

success, image = cap.read()
cv2.imshow('frame', image)

while success:
    frameId = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
    if frameId % int(frameRate/2) == 0:
        cv2.imwrite(PATH_SAVE_FRAME + "/frame{:.4f}s.jpg".format(frameId/frameRate), image)  # save frame as JPEG file
        data[frameId/frameRate] = [0, None, None]

    success, image = cap.read()
    cv2.imshow('frame', image)

    key = cv2.waitKey(50)
    if key == 27:
        break
    elif key == ord('p'):
        print('Pause at {}s'.format(frameId / frameRate))
        point_selected = False
        cv2.waitKey(0)  # Make video pause
        if point_selected:
            n = int(input('How many bb ? '))
            if n > args.max_bb:
                continue
            else:
                cv2.imwrite(PATH_SAVE_FRAME + "/frame{:.4f}s.jpg".format(frameId / frameRate), image)
                data[frameId / frameRate] = []
                for i in range(n):
                    label = int(input('1 for enemy - 2 for ally : '))
                    # Upper Left
                    data[frameId / frameRate] += [label, point]
                    print(point)
                    print(data[frameId / frameRate])
                    point_selected = False
                    cv2.waitKey(0)

                    # Bottom Right
                    data[frameId / frameRate].append(point)
                    print(point)
                    print(data[frameId / frameRate])
                    point_selected = False
                    cv2.waitKey(0)
                print("Finished frame")

                with open(os.path.join(PATH_SAVE, 'labels.json'), 'w') as f:
                    json.dump(data, f)


cap.release()
cv2.destroyAllWindows()

with open(os.path.join(PATH_SAVE, 'labels.json'), 'w') as f:
    json.dump(data, f)

