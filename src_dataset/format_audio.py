# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import re
import pdb
import argparse
import json
from pydub import AudioSegment


parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
args = parser.parse_args()


subpath = os.path.dirname(os.getcwd())
path_data = os.path.join(subpath, 'data')
path_dataset = os.path.join(subpath, 'dataset.json')
path_video = os.path.join(path_data, args.video_name)
audio_file = os.path.join(path_video, 'audio.wav')
path_audio_chunk = os.path.join(path_video, 'audio_chunk')

if not os.path.exists(path_audio_chunk):
    os.mkdir(path_audio_chunk)

with open(path_dataset, 'r') as fj:
    dataset = json.load(fj)


audio = AudioSegment.from_wav(audio_file)


for idx, t in enumerate(dataset[args.video_name].keys()):
    # break loop if at last element of list
    if idx == len(dataset[args.video_name]):
        break

    start = float(dataset[args.video_name][t]['startTime']) * 1000 - 1000  # pydub works in millisec
    end = float(dataset[args.video_name][t]['endTime']) * 1000 + 1000  # pydub works in millisec
    print("split at [{} : {}] ms".format(start, end))
    audio_chunk = audio[start: end]

    name = "audio_chunk_{}.wav".format(idx)
    path_save = os.path.join(path_audio_chunk, name)
    audio_chunk.export(path_save, format="wav")

    dataset[args.video_name][t]["audio"] = re.sub(r'/Volumes/LaCie/Inria/vizion/avp', r'.', path_save)


with open(path_dataset, 'w') as fj:
    json.dump(dataset, fj)


