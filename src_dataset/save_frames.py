# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import cv2
import argparse
import json
import time


parser = argparse.ArgumentParser()
parser.add_argument('--video_name', type=str, required=True)
args = parser.parse_args()

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
PATH_VIDEO = os.path.join(PATH_PROJECT, r'videos', args.video_name)
PATH_SAVE = os.path.join(PATH_DATA, args.video_name)
PATH_SAVE_FRAME = os.path.join(PATH_SAVE, 'new_frames')

if not os.path.exists(PATH_SAVE):
    os.mkdir(PATH_SAVE)

if not os.path.exists(PATH_SAVE_FRAME):
    os.mkdir(PATH_SAVE_FRAME)

# Video treatment
cap = cv2.VideoCapture(PATH_VIDEO)
time_vid = cap.get(cv2.CAP_PROP_POS_MSEC)

success, image = cap.read()
cv2.imshow('frame', image)
iter_frame = 1

while success:

    time_vid = cap.get(cv2.CAP_PROP_POS_MSEC)
    # print(time_vid)
    if time_vid % 500 < 33:
        print(time_vid)
        cv2.imwrite(PATH_SAVE_FRAME + "/frame{}.jpg".format(iter_frame), image)
        iter_frame += 1

        if not iter_frame % 100:
            print("\n {} frames saved \n".format(iter_frame))

    success, image = cap.read()
    cv2.imshow('frame', image)
    key = cv2.waitKey(1)


print('Out of loop')
cap.release()
cv2.destroyAllWindows()



