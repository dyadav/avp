# -*- coding: utf-8 -*-

"""
Created on Sat Oct 19 15:47:39 2019

@author: matthieufuteral-peter
"""

import os
import json
import shutil
from tqdm import tqdm

PATH_PROJECT = os.path.dirname(os.getcwd())
PATH_DATA = os.path.join(PATH_PROJECT, 'data')
PATH_TRAIN = os.path.join(PATH_PROJECT, 'train_set', 'train_set.json')
PATH_DEV = os.path.join(PATH_PROJECT, 'dev_set', 'dev_set.json')
PATH_TEST = os.path.join(PATH_PROJECT, 'test_set', 'test_set.json')

video_names = os.listdir(PATH_DATA)
try:
    video_names.remove(r'.DS_Store')
except:
    pass

assert len(video_names) == 8


def open_jfile(path):
    with open(path, 'r') as fj:
        return json.load(fj)


def copy_img(ids, dst, video_list=video_names):
    for id in tqdm(ids.keys()):
        idx = id.split('_')
        vid_name = video_list[int(idx[0]) - 1]
        path = os.path.join(PATH_DATA, vid_name, 'frames', idx[0] + r'_frame' + idx[-1] + r's.jpg')
        dest = os.path.join(dst, idx[0] + r'_frame' + idx[-1] + r's.jpg')
        shutil.copy(path, dest)
    pass


train_ids = open_jfile(PATH_TRAIN)
dev_ids = open_jfile(PATH_DEV)
test_ids = open_jfile(PATH_TEST)

copy_img(train_ids, os.path.join(os.path.dirname(PATH_TRAIN), 'frames'))
copy_img(dev_ids, os.path.join(os.path.dirname(PATH_DEV), 'frames'))
copy_img(test_ids, os.path.join(os.path.dirname(PATH_TEST), 'frames'))
